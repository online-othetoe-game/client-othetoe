import javax.swing.UIManager;

import view.pages.MainWindow;
import java.awt.*;

public class App {
    public static void main(String[] args) throws Exception {
        setUIFont(new javax.swing.plaf.FontUIResource("Poppins",Font.PLAIN,12));
        new MainWindow();
    }

    public static void setUIFont(javax.swing.plaf.FontUIResource f) {
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof javax.swing.plaf.FontUIResource)
                UIManager.put(key, f);
        }
    }
}
