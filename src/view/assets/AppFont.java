package view.assets;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class AppFont {
  public static Font getFont() {
    Font font = null;
    try {
      font = Font.createFont(Font.TRUETYPE_FONT, new File("res/font/Poppins-Regular.ttf"));
      GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
      ge.registerFont(font);
      // String[] a = ge.getAvailableFontFamilyNames();
      // for(String i : a ){
      //   System.out.println(i);
      // }
      // return font;
    } catch (IOException | FontFormatException e) {

    }
    return font;
  }

}
