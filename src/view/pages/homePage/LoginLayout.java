package view.pages.homePage;

import javax.swing.*;
import java.awt.*;

import javax.swing.plaf.InsetsUIResource;
import view.component.PasswordField;
import view.component.PrimaryButton;
import view.component.SecondaryButton;
import view.hooks.GlobalVar;

public class LoginLayout extends JPanel{
  private Object parent;

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();


  public LoginLayout(){
    this.setLayout(gbl);
    render();
  }

  public LoginLayout(Object parent){
    this();
    this.parent = parent;
    render();
  }

  public void render(){
    TextField usernameField= new TextField();
    PasswordField passwordField = new PasswordField();
    PrimaryButton loginButton = new PrimaryButton("Loggin");
    SecondaryButton registerButton = new SecondaryButton("Register");

    // layout
    gbl.columnWidths = new int[] {100, 300};
    gbl.rowWeights = new double[] {1};
    
    gbl.rowHeights = new int[] {40,35,35,40};
    gbc.insets = new InsetsUIResource(2, 2, 2, 2);
    gbc.fill = gbc.BOTH;
    
    gbc.gridy = 0;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    this.add(new JLabel("Welocome to Othetoe"), gbc);
    gbc.gridwidth = 1;

    gbc.gridy = 1;
    gbc.gridx = 0;
    this.add(new JLabel("Username"), gbc);
    gbc.gridx = 1;
    this.add(usernameField, gbc);

    gbc.gridy = 2;
    gbc.gridx = 0;
    this.add(new JLabel("Password"), gbc);
    gbc.gridx = 1;
    this.add(passwordField, gbc);

    gbc.gridy = 3;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    JPanel butonLayout = new JPanel();
    GridLayout gl = new GridLayout();
    gl.setHgap(5);
    butonLayout.setLayout(gl);
    butonLayout.add(registerButton);
    butonLayout.add(loginButton);
    this.add(butonLayout, gbc);
    
    //callback

    registerButton.addActionListener(
      (e)->{
        if(parent instanceof HomePage){
          HomePage p = (HomePage) parent;
          p.transition(p.registerLayout);
        }
      }
    );

    loginButton.addActionListener(
      (e)->{
        if(parent instanceof HomePage){
          HomePage p = (HomePage) parent;
          GlobalVar.isLogedIn = true;
          p.transition(p.homeLayout);
        }
      }
    );
  }
  
  public void transition(Component component){
    this.removeAll();
    this.revalidate();
    this.repaint();
    this.add(component);
  }
}