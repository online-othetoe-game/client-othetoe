package view.pages.homePage;

import javax.swing.*;
import view.hooks.GlobalVar;
import view.pages.gameplayPage.CreateGameLayout;
import view.pages.gameplayPage.GamePlayPage;
import view.pages.gameplayPage.OnGameLayout;

import java.awt.*;

public class HomePage extends JPanel{
  private Object parent;
  
  public HomeLayout homeLayout = new HomeLayout(this);
  public LoginLayout loginLayout = new LoginLayout(this);
  public RegisterLayout registerLayout = new RegisterLayout(this);
  public OnGameLayout onGameLayout;
  public GamePlayPage gamePlayPage;


  public HomePage(){
    render();
  }

  public HomePage(Object parent){
    this.parent = parent;
    
    gamePlayPage = new GamePlayPage(parent);
    
    render();
  }

  public void render(){
    if(!GlobalVar.isLogedIn){
      this.transition(loginLayout);
    }
    else{
      transition(homeLayout);
    }

  }

  public void transition(Component component){
    this.removeAll();
    this.revalidate();
    this.repaint();
    this.add(component);
  }

  
}
