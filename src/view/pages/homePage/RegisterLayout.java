package view.pages.homePage;

import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import java.awt.*;


import javax.swing.*;
import java.awt.*;
import view.component.*;
import view.component.TextField;

public class RegisterLayout extends JPanel {
  private Object parent;

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  public RegisterLayout() {
    render();
  }

  public RegisterLayout(Object parent) {
    this();
    this.parent = parent;
  }

  public void render(){
    this.setLayout(gbl);
    TextField fullnameField = new TextField();
    TextField usernameField= new TextField();
    PasswordField passwordField = new PasswordField();
    PrimaryButton backButton = new PrimaryButton("Back");
    SecondaryButton registerButton = new SecondaryButton("Register Now");

    // layout
    gbl.columnWidths = new int[] {100, 300};
    gbl.rowWeights = new double[] {1};
    gbl.rowHeights = new int[] {45,35,35,35,45};
    // component
    gbc.insets = new InsetsUIResource(2, 2, 2, 2);
    gbc.fill = gbc.BOTH;
    
    gbc.gridy = 0;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    this.add(new JLabel("Register to Othetoe"), gbc);
    gbc.gridwidth = 1;

    gbc.gridy = 1;
    gbc.gridx = 0;
    this.add(new JLabel("Full Name") , gbc);
    gbc.gridx = 1;
    this.add(fullnameField, gbc);

    gbc.gridy = 2;
    gbc.gridx = 0;
    this.add(new JLabel("Username"), gbc);
    gbc.gridx = 1;
    this.add(usernameField, gbc);

    gbc.gridy = 3;
    gbc.gridx = 0;
    this.add(new JLabel("Password"), gbc);
    gbc.gridx = 1;
    this.add(passwordField, gbc);

    gbc.gridy = 4;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    JPanel butonLayout = new JPanel();
    GridLayout gl = new GridLayout();
    gl.setHgap(5);
    butonLayout.setLayout(gl);
    butonLayout.add(backButton);
    butonLayout.add(registerButton);
    this.add(butonLayout, gbc);


    backButton.addActionListener(
      (e)->{
        if(parent instanceof HomePage){
          HomePage p = (HomePage) parent;
          p.transition(p.loginLayout);
        }
      }
    );

  }

}
