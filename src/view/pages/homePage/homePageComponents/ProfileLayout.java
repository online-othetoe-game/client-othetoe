package view.pages.homePage.homePageComponents;

import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import model.Player;
import view.component.PlayerAvatar;
import view.component.PrimaryButton;
import view.component.SecondaryButton;
import view.hooks.TestVar;
import view.pages.gameplayPage.CreateGameLayout;
import view.pages.homePage.HomePage;
import view.pages.homePage.LoginLayout;

import java.awt.*;

/**
 * ProfilePage
 */
public class ProfileLayout extends JPanel {
  private Object parent;

  private Player player = TestVar.player;

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  public ProfileLayout(Object parent) {
    this.parent = parent;
    render();
  }

  public void render() {
    this.setLayout(gbl);
    PlayerAvatar avatar = new PlayerAvatar(player.getAvatar());
    JLabel userLabel = new JLabel(String.format("%1$s [#%2$s]", player.getFullName(), player.getUsername()));
    JLabel winrateLabel = new JLabel(player.getWinRate() + "% Win Rate");
    JLabel winLabel = new JLabel("Wins : " + player.getWins());
    JLabel drawLabel = new JLabel("Draw : " + player.getDraws());
    JLabel lostLavel = new JLabel("Lost : " + player.getLosts());
    SecondaryButton logoutButton = new SecondaryButton("Logout");

    gbl.rowWeights = new double[] {1,1,1,1};
    // gbl.columnWeights = new double[] {1,1,1,1};
    gbl.columnWidths = new int[] {150,1000};

    gbc.insets = new InsetsUIResource(5, 5, 5, 5);
    gbc.fill = gbc.BOTH;
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridheight = 3;
    this.addComponent(avatar);
    gbc.gridheight = 1;
    

    gbc.gridx = 1;
    gbc.gridy = 0;
    this.addComponent(userLabel);
    gbc.gridy = 1;
    this.addComponent(winrateLabel);

    gbc.gridy = 2;
    JPanel statisticPanel = new JPanel(new GridLayout(1,4,2,2));
    statisticPanel.add(winLabel);
    statisticPanel.add(drawLabel);
    statisticPanel.add(lostLavel);
    statisticPanel.add(logoutButton);
    this.addComponent(statisticPanel);

    logoutButton.addActionListener(
      (e)->{
        if(parent instanceof HomePage){
          HomePage h = (HomePage) parent;
          h.transition(new LoginLayout(h));
        }
      }
    );
  }

  public void addComponent(Component comp) {
    this.add(comp, gbc);
  }
}