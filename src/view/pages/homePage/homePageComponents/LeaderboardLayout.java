package view.pages.homePage.homePageComponents;

import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import view.component.PrimaryButton;
import view.hooks.TestVar;

import java.awt.*;

public class LeaderboardLayout extends JPanel {

  public LeaderboardLayout() {
    render();
  }

  public LeaderboardLayout(LayoutManager layout) {
    super(layout);
    render();
  }

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  void render(){
    this.setLayout(gbl);
    TextField filterField = new TextField();
    PrimaryButton filterButton = new PrimaryButton("Search");

    PrimaryButton prevButton = new PrimaryButton("<");
    PrimaryButton nextButton = new PrimaryButton(">");

    gbl.columnWidths  = new int[] {400};
    gbl.rowWeights = new double[] {1,100,1};

    gbc.fill = gbc.BOTH;
    gbc.insets = new InsetsUIResource(2, 2, 2, 2);

    gbc.gridy = 0;
    gbc.gridx = 0;
    this.addComponent(filterField);
    gbc.gridx = 1;
    this.addComponent(filterButton);

    gbc.gridy = 1;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    this.addComponent(new LeaderboardList(TestVar.players) );
    gbc.gridwidth = 1;

    gbc.gridy = 2;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    JPanel paginationPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    paginationPanel.add(prevButton);
    paginationPanel.add(nextButton);
    this.addComponent(paginationPanel);


  }
  public void addComponent(Component comp) {
    this.add(comp, gbc);
  }
}
