package view.pages.homePage.homePageComponents;

import javax.swing.JPanel;
import javax.swing.text.PlainDocument;

import model.Game;
import model.Player;
import view.component.AvaiableGameItem;
import view.component.LeaderboardPlayerItem;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

public class LeaderboardList  extends JPanel{
  ArrayList<Player> players;
  
  public LeaderboardList(ArrayList<Player> players){
    this.players = players;
    render();
  }
  
  public void render(){
    
    for(Player player: players){
      this.add(new LeaderboardPlayerItem(player));
    }
    
    this.setLayout(new GridLayout(5,2, 2, 2));
  }
}
