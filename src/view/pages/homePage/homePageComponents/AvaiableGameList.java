package view.pages.homePage.homePageComponents;

import javax.swing.JPanel;

import model.Game;
import view.component.AvaiableGameItem;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

public class AvaiableGameList  extends JPanel{
  ArrayList<Game> games;
  
  public AvaiableGameList(ArrayList<Game> games){
    this.games = games;
    render();
  }
  
  public void render(){
    this.setLayout(new GridLayout(5,2, 2, 2));
    
    for(Game game: games){
      this.add(new AvaiableGameItem(game));
    }
  }
}
