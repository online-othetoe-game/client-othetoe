package view.pages.homePage.homePageComponents;

import javax.security.auth.x500.X500Principal;
import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import view.component.PrimaryButton;
import view.component.TextField;
import view.hooks.TestVar;
import view.pages.gameplayPage.CreateGameLayout;
import view.pages.gameplayPage.GamePlayPage;
import view.pages.homePage.HomePage;

import java.awt.*;

public class AvaiableGameLayout extends JPanel {
  private Object parent;

  public AvaiableGameLayout() {
    render();
  }

  public AvaiableGameLayout(Object parent) {
    this.parent = parent;
    render();
  }

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  void render() {
    this.setLayout(gbl);

    PrimaryButton startNewGameButton = new PrimaryButton("Start new Game");
    PrimaryButton joinRandomGameButton = new PrimaryButton("Join Random Game");
    TextField filterField = new TextField();
    PrimaryButton filterButton = new PrimaryButton("Filter");
    AvaiableGameList avaiableGameList = new AvaiableGameList(TestVar.games);

    PrimaryButton prevButton = new PrimaryButton("<");
    PrimaryButton nextButton = new PrimaryButton(">");


    gbl.columnWidths  = new int[] {400};
    gbl.rowWeights = new double[] {1,1,1,1,100,1};
  
    gbc.fill = gbc.BOTH;
    gbc.insets = new InsetsUIResource(2, 2, 2, 2);

    gbc.gridy = 0;
    gbc.gridx = 0;
    gbc.gridwidth =2;
    this.addComponent(startNewGameButton);

    gbc.gridy = 1;
    this.addComponent(joinRandomGameButton);

    gbc.gridy = 2;
    this.addComponent(new JLabel("Filter avaiable game by name"));

    gbc.gridy = 3;
    gbc.gridwidth = 1;
    this.addComponent(filterField);
    gbc.gridx = 1;
    this.addComponent(filterButton);

    gbc.gridy = 4;
    gbc.gridx =0;
    gbc.gridwidth = 2;
    this.addComponent(avaiableGameList);

    gbc.gridy = 5;
    gbc.gridx = 0;
    JPanel paginationPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    paginationPanel.add(prevButton);
    paginationPanel.add(nextButton);
    this.addComponent(paginationPanel);

    startNewGameButton.addActionListener(
      (e)->{
        if(parent instanceof HomePage){
          HomePage h = (HomePage) parent;
          h.transition(new GamePlayPage(h));
        }
      }
    );
  }

  public void addComponent(Component comp) {
    this.add(comp, gbc);
  }
}
