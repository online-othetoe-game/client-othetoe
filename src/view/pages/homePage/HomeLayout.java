package view.pages.homePage;

/**
 * HomePage
 */
import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import view.component.TextField;
import view.pages.homePage.homePageComponents.AvaiableGameLayout;
import view.pages.homePage.homePageComponents.LeaderboardLayout;
import view.pages.homePage.homePageComponents.ProfileLayout;

import java.awt.*;
import javax.swing.*;

public class HomeLayout extends JPanel{
  private Object parent;

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  public HomeLayout homeLayout;
  public LoginLayout loginLayout = new LoginLayout(this);
  public RegisterLayout registerLayout = new RegisterLayout(this);


  public HomeLayout(){
    render();
  }

  public HomeLayout(Object parent){
    this.parent = parent;
    render();
  }

  void render(){
    this.setLayout(gbl);

    setBorder(BorderFactory.createEmptyBorder(0,0,0,0) );
    
    ProfileLayout profileLayout = new ProfileLayout(parent);
    // profileLayout.setBorder(BorderFactory.createLineBorder(Color.black, 1) );
    AvaiableGameLayout avaiableGameLayout = new AvaiableGameLayout(parent);
    // avaiableGameLayout.setBorder(BorderFactory.createLineBorder(Color.black, 1) );
    LeaderboardLayout leaderboardGameLayout = new LeaderboardLayout();
    // leaderboardGameLayout.setBorder(BorderFactory.createLineBorder(Color.black, 1) );

    gbl.rowHeights = new int[] {150, 450};
    gbl.rowWeights = new double[] {1,1};
    // gbl.columnWidths = new int[] {550,550};
    // gbl.columnWeights = new double[] {1,1};

    gbc.insets = new InsetsUIResource(2, 2, 2, 2);
    gbc.fill = gbc.BOTH;
    gbc.gridy= 0;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    this.addComponent(profileLayout);
    gbc.gridwidth = 1;

    gbc.gridy= 1;
    gbc.gridx = 0;
    this.addComponent(leaderboardGameLayout);
    gbc.gridx = 1;
    this.addComponent(avaiableGameLayout);

  }

  void addComponent(Component component){
    this.add(component, gbc);
  }
  

  
}