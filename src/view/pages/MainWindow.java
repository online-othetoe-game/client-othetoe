package view.pages;
import javax.naming.ldap.ManageReferralControl;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;

import view.assets.AppFont;
import view.pages.gameplayPage.OnGameLayout;
import view.pages.homePage.HomePage;

import java.awt.*;

public class MainWindow extends JFrame{
  static JPanel mainPanel = new JPanel();
  
  public MainWindow(){
    this.setTitle("Othetoe Game");
    this.setSize(1220, 650);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    mainPanel.setBorder(BorderFactory.createEmptyBorder(0,0,0,0) );
    this.setContentPane(mainPanel);
    this.setResizable(false);
    this.setVisible(true);
    trasitionTo(new HomePage(mainPanel));

    AppFont.getFont();
  }

  public void trasitionTo(Component component){
    mainPanel.removeAll();
    mainPanel.add(component);
    mainPanel.revalidate();
    mainPanel.repaint();
  }

  public static void staticTrasitionTo(Component component){
    mainPanel.removeAll();
    mainPanel.add(component);
    mainPanel.revalidate();
    mainPanel.repaint();
  }
}
