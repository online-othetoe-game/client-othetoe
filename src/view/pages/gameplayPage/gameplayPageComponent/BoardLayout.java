package view.pages.gameplayPage.gameplayPageComponent;


import javax.swing.JPanel;

import model.Cell;

import java.awt.*;
import javax.swing.*;

public class BoardLayout extends JPanel{
  Object parent;
  int size;

  public BoardLayout(Object parent, int size) {
    this.size = size;
    this.parent = parent;
    render();
  }

  public BoardLayout( int size) {
    this.size = size;
    render();
  }

  public BoardLayout(Object parent) {
    this.parent = parent;
    render();
  }

  void render(){
    this.setLayout(new GridLayout(size, size));
    for (int i = 0; i<size; i++){
      for(int j = 0; j<size; j++){
        this.add( new Cell(i, j) );
      }
    }
  }


  
}
