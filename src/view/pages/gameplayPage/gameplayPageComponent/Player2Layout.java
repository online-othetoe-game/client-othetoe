package view.pages.gameplayPage.gameplayPageComponent;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.InsetsUIResource;

import model.Player;
import view.assets.AppFont;
import view.component.PlayerAvatar;

import java.awt.*;

public class Player2Layout extends JPanel{
  Object parent;

  private int points = 0;
  private int pointsToWin = 0;
  Player player;

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  // statefull variable
  JLabel pointLabel = new JLabel(""+points);

  public Player2Layout(Object parent, Player player){
    this.parent = parent;
    this.player = player;
    render();
  }

  void render(){
    this.setBorder(BorderFactory.createLineBorder(Color.black));
    this.setLayout(gbl);
    pointLabel.setFont( new FontUIResource("Poppins", Font.BOLD, 70 ) );
    pointLabel.setHorizontalAlignment(SwingConstants.CENTER);
    
    JLabel youLabel = new JLabel("OPPONENT");
    youLabel.setFont( new FontUIResource("Poppins", Font.BOLD, 15) );

    JLabel playerNameLabel = new JLabel(player.getFullName() );
    JLabel playerUsernameLabel = new JLabel("#"+player.getUsername());
    JLabel pointWinLabel = new JLabel("Out of "+ this.pointsToWin);

    playerNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
    playerUsernameLabel.setHorizontalAlignment(SwingConstants.CENTER);
    pointWinLabel.setHorizontalAlignment(SwingConstants.CENTER);

    gbl.rowHeights = new int[]    {30, 200, 30, 30, 70, 30};
    gbl.columnWeights = new double[] {1};

    gbc.fill = gbc.BOTH;
    gbc.insets = new InsetsUIResource(5, 5, 5, 5);

    gbc.gridy = 0;
    gbc.gridx = 0;
    this.addComponent (youLabel);

    gbc.gridy = 1;
    this.addComponent (new PlayerAvatar( player.getAvatar() ));

    gbc.gridy = 2;
    this.addComponent (playerNameLabel);

    gbc.gridy = 3; 
    this.addComponent (playerUsernameLabel);

    gbc.gridy = 4; 
    this.addComponent (pointLabel);

    gbc.gridy = 5; 
    this.addComponent (pointWinLabel);
  }

  public void setPointsToWin(int pointsToWin) {
    this.pointsToWin = pointsToWin;
  }

  public int getPointsToWin() {
    return pointsToWin;
  }

  public int getPoints() {
    return points;

  }

  public void setPoints(int points) {
    this.points = points;
    pointLabel.setText(""+ points);
  }

  void addComponent(Component component){
    this.add(component, gbc);
  }
}
