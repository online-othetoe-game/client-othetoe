package view.pages.gameplayPage.gameplayPageComponent;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;


import view.component.SecondaryButton;
import view.pages.homePage.HomeLayout;
import view.pages.homePage.HomePage;

import java.awt.*;
import javax.swing.*;

public class Footer extends JPanel {
  Object parent;
  public Footer(Object parent) {
    this.parent = parent;
    render();
  }

  private JLabel infoLabel = new JLabel("Game Started");

  void render() {
    this.setBorder(BorderFactory.createLineBorder(Color.black));
    SecondaryButton surrenderButton = new SecondaryButton("Surrender");
    this.setLayout(new FlowLayout(FlowLayout.LEFT));
    this.add(surrenderButton);
    this.add(infoLabel);

    surrenderButton.addActionListener(
      (e)->{
        if(parent instanceof HomePage){
          HomePage h = (HomePage) parent;
          h.transition(new HomeLayout(h));
        }
      }
    );
  }

  public void setInfo(String info){
    infoLabel.setText(info);
  }


}
