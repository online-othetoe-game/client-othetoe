package view.pages.gameplayPage;

import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import model.Game;
import model.Player;
import view.component.ComboBox;
import view.component.IntegerTextField;
import view.component.PasswordField;
import view.component.PrimaryButton;
import view.component.SecondaryButton;
import view.hooks.GlobalVar;
import view.hooks.TestVar;
import view.pages.homePage.HomeLayout;
import view.pages.homePage.HomePage;

import java.awt.*;

public class CreateGameLayout extends JPanel {
  private Object parent;

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  // game setup
  private String[] gridsStr = new String[] { "10 x 10", "15 x 15", "20 x 20", "25 x 25", "30 x 30" };
  private int[] gridInt = new int[] { 10, 15, 20, 25, 30 };

  private String[] straightStr = new String[] { "4 Tiles", "5 Tiles", "6 Tiles", "7 Tiles", "8 Tiles" };
  private int[] straightInt = new int[] { 4, 5, 6, 7, 8 };

  public CreateGameLayout(Object parent) {
    this.parent = parent;
    render();
  }

  void render() {
    this.setLayout(gbl);
    TextField gameNameField = new TextField();
    
    IntegerTextField pointToWin = new IntegerTextField();
    JComboBox<String> gridComboBox = new JComboBox<String>(gridsStr);
    JComboBox<String> straightComboBox = new JComboBox<String>(straightStr);

    SecondaryButton backButton = new SecondaryButton("Back");
    PrimaryButton startGameButton = new PrimaryButton("Start Game Now");

    // layout
    gbl.columnWidths = new int[] { 100, 300 };
    gbl.rowWeights = new double[] { 1 };
    gbl.rowHeights = new int[] { 45, 35, 35, 35, 35, 45 };
    // component
    gbc.insets = new InsetsUIResource(2, 2, 2, 2);
    gbc.fill = gbc.BOTH;

    gbc.gridy = 0;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    this.add(new JLabel("Game Setup"), gbc);
    gbc.gridwidth = 1;

    gbc.gridy = 1;
    gbc.gridx = 0;
    this.add(new JLabel("Game name"), gbc);
    gbc.gridx = 1;
    this.add(gameNameField, gbc);

    gbc.gridy = 2;
    gbc.gridx = 0;
    this.add(new JLabel("Point to Win"), gbc);
    gbc.gridx = 1;
    this.add(pointToWin, gbc);

    gbc.gridy = 3;
    gbc.gridx = 0;
    this.add(new JLabel("Straight Tiles"), gbc);
    gbc.gridx = 1;
    this.add(straightComboBox, gbc);

    gbc.gridy = 4;
    gbc.gridx = 0;
    this.add(new JLabel("Board Grid Size"), gbc);
    gbc.gridx = 1;
    this.add(gridComboBox, gbc);

    gbc.gridy = 5;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    JPanel butonLayout = new JPanel();
    GridLayout gl = new GridLayout();
    gl.setHgap(5);
    butonLayout.setLayout(gl);
    butonLayout.add(backButton);
    butonLayout.add(startGameButton);
    this.add(butonLayout, gbc);

    backButton.addActionListener(
      (e)->{
        if(parent instanceof HomePage){
          HomePage h = (HomePage) parent;
          h.transition(new HomeLayout(h));
        }
      }
    );

    startGameButton.addActionListener(
      (e)->{
        String gameName = gameNameField.getText();
        int grids = gridInt[gridComboBox.getSelectedIndex()];
        int straight = straightInt[straightComboBox.getSelectedIndex()];
        int point = ((Number)pointToWin.getValue()).intValue();

        Game game = new Game(gameName, TestVar.player , grids, point, straight);

        if(parent instanceof HomePage){
          HomePage h = (HomePage) parent;
          h.transition(new OnGameLayout(h, game));
        }
      }
    );
  }

  public void transition(Component component) {
    this.removeAll();
    this.revalidate();
    this.repaint();
    this.add(component);
  }
}
