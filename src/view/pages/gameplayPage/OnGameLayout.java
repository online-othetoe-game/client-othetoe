package view.pages.gameplayPage;

import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.text.AttributeSet.ColorAttribute;

import model.Game;
import model.Player;
import view.pages.gameplayPage.gameplayPageComponent.BoardLayout;
import view.pages.gameplayPage.gameplayPageComponent.Footer;
import view.pages.gameplayPage.gameplayPageComponent.Player1Layout;
import view.pages.gameplayPage.gameplayPageComponent.Player2Layout;

import java.awt.*;

public class OnGameLayout extends JPanel {
  Object parent;
  Game game;

  public OnGameLayout(Object parent, Game game) {
    this.game = game;
    this.parent = parent;
    render();
  }

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  void render() {
    this.setLayout(gbl);

    gbl.rowHeights = new int[] { 570, 30 };
    gbl.columnWidths = new int[] { 300, 570, 300 };

    gbl.rowWeights = new double[] {1};
    gbl.columnWeights = new double[] {1};

    Player1Layout player1Layout = new Player1Layout(parent, new Player("Lord Udin", "udin20", "udins avatar"));
    Player2Layout player2Layout = new Player2Layout(parent, new Player("Lord Dyne", "dyne", "dynes avatar"));
    BoardLayout boardLayout = new BoardLayout(parent, game.getGrids());
    Footer footer = new Footer(parent);

    gbc.fill = 1;
    gbc.insets = new InsetsUIResource(2, 2, 2, 2);

    gbc.gridy = 0;
    gbc.gridx = 0;
    this.addComponent(player1Layout);
    gbc.gridx = 1;
    this.addComponent(boardLayout);
    gbc.gridx = 2;
    this.addComponent(player2Layout);

    gbc.gridy = 1;
    gbc.gridx = 0;
    gbc.gridwidth = 3;
    this.addComponent(footer);

  }

  void addComponent(Component component) {
    this.add(component, gbc);
  }

}
