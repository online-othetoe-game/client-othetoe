package view.pages.gameplayPage;

import javax.swing.*;

import model.Game;

import java.awt.*;

public class GamePlayPage extends JPanel{
  Object parent;
  boolean isStarted = false;


  public GamePlayPage(Object parent){
    this.parent = parent;
    render();
  }

  public GamePlayPage(Object parent, boolean isStarted){
    this.parent = parent;
    this.isStarted = isStarted;
    render();
  }

  void render(){
    this.transition(new CreateGameLayout(parent));
  }

  public void transition(Component component) {
    this.removeAll();
    this.revalidate();
    this.repaint();
    this.add(component);
  }

  
}
