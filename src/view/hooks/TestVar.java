package view.hooks;

import java.util.ArrayList;

import model.Game;
import model.Player;

public class TestVar {
  public static Player player = new Player("Lord Udin", "lordUdin20", "avatar udin");
  public static ArrayList<Game> games = getGameData();
  public static ArrayList<Player> players = getPlayerData();

  static ArrayList<Game> getGameData() {
    ArrayList<Game> g = new ArrayList<>();

    for (int i = 0; i < 5; i++) {
      g.add(
          new Game("game $" + i,
              new Player("player y" + i, "playery" + "i", "avatary" + "i", i * 100, i * 100 - 30, 10, 10), i * 5,
              i * 50, i));
    }

    for (int i = 0; i < 5; i++) {
      g.add(
          new Game("game $" + i, new Player("player x" + i, "playerx" + i + "x", "avatarx" + i), i * 5, i * 50, i));
    }

    return g;
  }

  static ArrayList<Player> getPlayerData() {
    ArrayList<Player> p = new ArrayList<>();

    for (int i = 0; i < 10; i++) {
      p.add(new Player("player y" + i, "playery" + "i", "avatary" + "i", i * 100, i * 100 - 30, 10, 10));
    }

    return p;
  }
}
