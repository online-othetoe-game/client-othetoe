package view.component;

import javax.swing.*;
import java.awt.*;

public class PrimaryButton extends JButton {

  public PrimaryButton() {
    render();
  }

  public PrimaryButton(Icon icon) {
    super(icon);
    render();

  }

  public PrimaryButton(String text) {
    super(text);
    render();

  }

  public PrimaryButton(Action a) {
    super(a);
    render();

  }

  public PrimaryButton(String text, Icon icon) {
    super(text, icon);
    render();
  }

  void render() {
    this.setBackground(Color.gray);
  }

}
