package view.component;

import java.util.ArrayList;

import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import java.awt.*;

import model.Game;
import model.Player;

public class AvaiableGameItem extends JPanel {
  private Game game;

  public AvaiableGameItem(Game game) {
    this.game = game;
    render();
  }

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  void render() {
    this.setLayout(gbl);
    this.setBorder(BorderFactory.createLineBorder(Color.gray));

    JLabel gameLabel = new JLabel(
        game.getGameName() + " [G/P/S : " + game.getGrids() + "/" + game.getPointToWin() + "/" + game.getStraight()
            + "]");

    SecondaryButton joinGameButton = new SecondaryButton("Join Game");
    Player player = game.getPlayer1();
    JLabel userLabel = new JLabel(String.format("%1$s [#%2$s]", player.getFullName(), player.getUsername()));
    JLabel userWinRate = new JLabel( player.getWinRate()+ "% Win rate" );

    // layout
    // gbl.columnWidths = new int[] {20, 200 };
    gbl.rowWeights = new double[] {1,1,1};
    gbl.columnWeights = new double[] {0,1};

    gbc.fill = gbc.BOTH;
    gbc.insets = new InsetsUIResource(2, 2, 2, 2);

    gbc.gridy = 0;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    this.addComponent(gameLabel);
    gbc.gridwidth  = 1;

    gbc.gridy = 1;
    gbc.gridx = 0;
    gbc.gridheight = 2;
    this.addComponent(joinGameButton);
    gbc.gridheight = 1;

    gbc.gridy = 1;
    gbc.gridx = 1;
    this.addComponent(userLabel);

    gbc.gridy = 2;
    gbc.gridx = 1;
    this.addComponent(userWinRate);
  }

  void addComponent(Component component) {
    this.add(component, gbc);
  }

}
