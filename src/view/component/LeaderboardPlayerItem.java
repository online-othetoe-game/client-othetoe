package view.component;

import javax.swing.*;
import javax.swing.plaf.InsetsUIResource;

import model.Player;
import view.component.PlayerAvatar;
import view.hooks.TestVar;

import java.awt.*;

/**
 * ProfilePage
 */
public class LeaderboardPlayerItem extends JPanel {
  private Object parent;

  private Player player;

  private GridBagLayout gbl = new GridBagLayout();
  private GridBagConstraints gbc = new GridBagConstraints();

  public LeaderboardPlayerItem(Player player) {
    this.player = player;
    render();
  }

  public LeaderboardPlayerItem(Object parent, Player player) {
    this.player = player;
    this.parent = parent;
    render();
  }

  public void render() {
    this.setLayout(gbl);
    this.setBorder(BorderFactory.createLineBorder(Color.gray));
    PlayerAvatar avatar = new PlayerAvatar(player.getAvatar());
    JLabel userLabel = new JLabel(String.format("%1$s [#%2$s]", player.getFullName(), player.getUsername()));
    JLabel winrateLabel = new JLabel(player.getWinRate() + "% Win Rate");
    JLabel winLabel = new JLabel("Wins : " + player.getWins());
    JLabel drawLabel = new JLabel("Draw : " + player.getDraws());
    JLabel lostLavel = new JLabel("Lost : " + player.getLosts());

    

    // gbl.rowWeights = new double[] {1,1,1,1};
    // gbl.columnWeights = new double[] {1,1,1,1};
    // gbl.columnWidths = new int[] {50,};
    // gbl.columnWidths = new int[] {20, 200 };
    gbl.rowWeights = new double[] {1,1,1};
    gbl.columnWeights = new double[] {0,1};

    gbc.insets = new InsetsUIResource(2, 2, 2, 2);
    gbc.fill = gbc.BOTH;
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridheight = 3;
    this.addComponent(avatar);
    gbc.gridheight = 1;
    

    gbc.gridx = 1;
    gbc.gridy = 0;
    this.addComponent(userLabel);
    gbc.gridy = 1;
    this.addComponent(winrateLabel);

    gbc.gridy = 2;
    JPanel statisticPanel = new JPanel(new GridLayout(1,3,2,2));
    statisticPanel.add(winLabel);
    statisticPanel.add(drawLabel);
    statisticPanel.add(lostLavel);
    this.addComponent(statisticPanel);
  }

  public void addComponent(Component comp) {
    this.add(comp, gbc);
  }
}