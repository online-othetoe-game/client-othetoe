package view.component;

import javax.swing.*;
import java.awt.*;

public class SecondaryButton extends JButton{

  public SecondaryButton() {
    render();
  }

  public SecondaryButton(Icon icon) {
    super(icon);
    render();

  }

  public SecondaryButton(String text) {
    super(text);
    render();

  }

  public SecondaryButton(Action a) {
    super(a);
    render();

  }

  public SecondaryButton(String text, Icon icon) {
    super(text, icon);
    render();

  }

  void render() {
    this.setBackground(Color.lightGray);
  }
  
}
