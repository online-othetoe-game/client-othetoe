package view.component;

import javax.swing.JPasswordField;
import javax.swing.text.Document;

public class PasswordField extends JPasswordField{

  public PasswordField(Document doc, String txt, int columns) {
    super(doc, txt, columns);
  }

  public PasswordField() {
  }

  public PasswordField(String text) {
    super(text);
  }

  public PasswordField(int columns) {
    super(columns);
  }

  public PasswordField(String text, int columns) {
    super(text, columns);
  }
  
}
