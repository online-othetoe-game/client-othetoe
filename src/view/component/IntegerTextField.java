package view.component;

import java.text.NumberFormat;

import javax.swing.JFormattedTextField;
import javax.swing.text.NumberFormatter;

public class IntegerTextField extends JFormattedTextField {
  public IntegerTextField(){
    super(new NumberFormatter(NumberFormat.getIntegerInstance()));
  }
}
