package view.component;

import javax.swing.*;
import java.awt.*;

public class PlayerAvatar extends JButton{

  public PlayerAvatar() {
  }

  public PlayerAvatar(Icon icon) {
    super(icon);
  }

  public PlayerAvatar(String text) {
    super(text);
  }

  public PlayerAvatar(Action a) {
    super(a);
  }

  public PlayerAvatar(String text, Icon icon) {
    super(text, icon);
  }
  
}
