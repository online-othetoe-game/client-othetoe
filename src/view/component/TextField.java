package view.component;

import javax.swing.JTextField;
import javax.swing.text.Document;

public class TextField extends JTextField {

  public TextField(Document doc, String text, int columns) {
    super(doc, text, columns);
  }

  public TextField() {
  }

  public TextField(String text) {
    super(text);
  }

  public TextField(int columns) {
    super(columns);
  }

  public TextField(String text, int columns) {
    super(text, columns);
  }
  
}
