package model;

import javax.swing.*;
import java.awt.*;

public class Cell extends JButton {
  private int iPos, jPos;

  public Cell(int iPos, int jPos) {
    this.iPos = iPos;
    this.jPos = jPos;
    render();
  }

  void render(){
    this.setBackground( Color.lightGray.brighter() );
  }

  public int getiPos() {
    return iPos;
  }

  public int getjPos() {
    return jPos;
  }

}
