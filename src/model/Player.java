package model;

public class Player {
  private String fullName;
  private String username;
  private String password;
  private String avatar;
  private int gamePlayed = 0;
  private int wins = 0;
  private int draws = 0;
  private int losts = 0;


  public Player(String fullName, String username, String avatar, int gamePlayed, int wins, int draws, int losts) {
    this.fullName = fullName;
    this.username = username;
    this.avatar = avatar;
    this.gamePlayed = gamePlayed;
    this.wins = wins;
    this.draws = draws;
    this.losts = losts;
  }

  public Player(String fullName, String username, String avatar) {
    this.fullName = fullName;
    this.username = username;
    this.avatar = avatar;
  }

  public Player(String fullName, String username, String password, String avatar) {
    this.fullName = fullName;
    this.username = username;
    this.password = password;
    this.avatar = avatar;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public int getGamePlayed() {
    return gamePlayed;
  }

  public void setGamePlayed(int gamePlayed) {
    this.gamePlayed = gamePlayed;
  }

  public int getWins() {
    return wins;
  }

  public void setWins(int wins) {
    this.wins = wins;
  }

  public int getDraws() {
    return draws;
  }

  public void setDraws(int draws) {
    this.draws = draws;
  }

  public int getLosts() {
    return losts;
  }

  public void setLosts(int losts) {
    this.losts = losts;
  }

  public int getWinRate(){
    if(gamePlayed == 0){
      return 0;
    }
    else if(wins == 0){
      return 0;
    }

    int winRate = 100*wins/gamePlayed;
    return winRate;
  }

}
