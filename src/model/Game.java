package model;

public class Game {
  Player player1;
  Player player2;

  private String gameName;
  private int grids;
  private int pointToWin;
  private int straight;

  public Game(String gameName, Player player1, int grids, int pointToWin, int straight) {
    this.gameName = gameName;
    this.player1 = player1;
    this.grids = grids;
    this.pointToWin = pointToWin;
    this.straight = straight;
  }

  public Game() {
  }

  public Player getPlayer1() {
    return player1;
  }

  public void setPlayer1(Player player1) {
    this.player1 = player1;
  }

  public Player getPlayer2() {
    return player2;
  }

  public void setPlayer2(Player player2) {
    this.player2 = player2;
  }

  public String getGameName() {
    return gameName;
  }

  public void setGameName(String gameName) {
    this.gameName = gameName;
  }

  public int getGrids() {
    return grids;
  }

  public void setGrids(int grids) {
    this.grids = grids;
  }

  public int getPointToWin() {
    return pointToWin;
  }

  public void setPointToWin(int pointToWin) {
    this.pointToWin = pointToWin;
  }

  public int getStraight() {
    return straight;
  }

  public void setStraight(int straight) {
    this.straight = straight;
  }

  

  

}
